// Operating Systems Principles Project 3: mandelmovie.cpp
// Andrea Augustin and Emily Koykka

// Includes
#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <cstring>
#include "bitmap.h"
#include "mandel.h"
using namespace std;

// Globals
int X = 0.286932;
int Y = 0.014287;
int S = 0.000005;
int M = 900;

// Help Function 
void help() {
	printf("Usage:  ./mandelmovie [PROCESSORS]\n");
	printf("\tPROCESSORS\t\t\tNumber of processors to run\n");
	exit(0);
}

int main(int argc, char *argv[]) {
	int num_bitmaps = 0;
	int processors = 0;
	int num_processors;
	int interval = (2 - S) / 50;
	int curr_s = 2;
	// Parse command line
	if (argc==2) {
		processors = atoi(argv[1]);
	}
	else {
		help();
	}

	while(num_bitmaps < 50) { // Continue forking and execing until all 50 bitmaps are created
		for(num_processors=0; num_processors<processors; num_processors++) { // Only have number of processes specified at a time
			int rc = fork();
			printf("rc: %d\n", rc);
			if(rc<0) { // Error catching
				printf("Error: fork failed\n");
				exit(1);
			}
			else if(rc==0) { // Child process exec mandel
				char *mandel_args[14]; // Make array for exec input
				char buffer [50];
				mandel_args[0] = strdup("mandel");
				mandel_args[1] = strdup("-x");
				snprintf(buffer, sizeof(buffer), "%d", X);
				mandel_args[2] = buffer;
				snprintf(buffer, sizeof(buffer), "%d", Y);
				mandel_args[3] = strdup("-y");
				mandel_args[4] = buffer;
				mandel_args[9] = strdup("-s");
				snprintf(buffer, sizeof(buffer), "%d", curr_s);
				mandel_args[10] = buffer;
				mandel_args[11] = strdup("-m");
				snprintf(buffer, sizeof(buffer), "%d", M);
				mandel_args[12] = buffer;
				mandel_args[13] = strdup("-o");
				snprintf(buffer, sizeof(buffer), "%d", num_bitmaps+1);
				mandel_args[14] = buffer;
				mandel_args[15] = NULL;

				curr_s -= interval;

				execvp(mandel_args[0], mandel_args); // Run mandel
			}
			else {
				break;
			}
		}
		pid_t p = wait(NULL); // Parent process
		if (p == -1) { // Error catching
			printf("Error: wait failed\n");
			exit(1);
		}
		else { // Parent waited
			num_processors--;
			num_bitmaps++;
		}
	}
	return 0;
}

